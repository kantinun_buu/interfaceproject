/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.animalinterface;

/**
 *
 * @author EAK
 */
public abstract class AquaticAnimal extends Animal implements Swimmable {

    public AquaticAnimal(String name) {
        super(name, 0);
    }

}
