/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.animalinterface;

/**
 *
 * @author EAK
 */
public class TestProgram {

    public static void main(String[] args) {
        Crocodile crocodile = new Crocodile("Croco");
        Snake snake = new Snake("Sneak");
        Human human = new Human("Dio");
        Dog dog = new Dog("Doge");
        Cat cat = new Cat("Mimi");
        Fish fish = new Fish("Aqua");
        Crab crab = new Crab("Holy");
        Bat bat = new Bat("Boi");
        Bird bird = new Bird("owo");
        Car car = new Car();
        Plane plane = new Plane();

        Crawlable[] crawlable = {crocodile, snake};
        for (Crawlable c : crawlable) {
            if (c instanceof Reptile) {
                Reptile r = (Reptile) c;
                r.eat();
                r.walk();
                r.crawl();
                r.speak();
                r.sleep();
            }
        }
        
        System.out.println("");
        
        Runable[] runable = {human, dog, cat, car, plane};
        for (Runable r : runable) {
            if (r instanceof LandAnimal) {
                LandAnimal l = (LandAnimal) r;
                l.eat();
                l.walk();
                l.run();
                l.speak();
                l.sleep();
            } else {
                if (r instanceof Car) {
                    Car c = (Car) r;
                    c.startEngine();
                    c.run();
                    c.stopEngine();
                } else if (r instanceof Plane) {
                    Plane p = (Plane) r;
                    p.startEngine();
                    p.run();
                    p.stopEngine();
                }
            }
        }
        
        System.out.println("");
        
        Swimmable[] swimmable = {fish, crab};
        for (Swimmable s : swimmable) {
            if (s instanceof AquaticAnimal) {
                AquaticAnimal a = (AquaticAnimal) s;
                a.eat();
                a.walk();
                a.swim();
                a.speak();
                a.sleep();
            }
        }
        
        System.out.println("");
        
        Flyable[] flyable = {bat, bird, plane};
        for (Flyable f : flyable) {
            if (f instanceof Poultry) {
                Poultry p = (Poultry) f;
                p.eat();
                p.walk();
                p.fly();
                p.speak();
                p.sleep();
            } else if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
                p.stopEngine();
            }
        }
    }

}
